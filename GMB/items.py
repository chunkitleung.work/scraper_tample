# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class GmbItem(scrapy.Item):
    # define the fields for your item here like:

    route_id = scrapy.Field()
    route_seq = scrapy.Field()
    stop_seq = scrapy.Field()
    gen_date = scrapy.Field()
    gen_time = scrapy.Field()
    eta_1_diff = scrapy.Field()
    eta_1_time = scrapy.Field()
    eta_1_remark = scrapy.Field()
    eta_2_diff = scrapy.Field()
    eta_2_time = scrapy.Field()
    eta_2_remark = scrapy.Field()
    eta_3_diff = scrapy.Field()
    eta_3_time = scrapy.Field()
    eta_3_remark = scrapy.Field()


