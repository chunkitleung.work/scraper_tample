# connect to the database 

cqlsh 158.132.175.131 -u cassandra -p cassandra


# create keyspace

CREATE KEYSPACE IF NOT EXISTS HKI_GMB WITH REPLICATION ={ 'class' :  'SimpleStrategy', 'replication_factor': '1'} AND durable_writes = true;

CREATE TABLE IF NOT EXISTS hki_gmb.eta_data (route_id int ,route_seq int , stop_seq int ,gen_date date, fetch_moment timestamp, gen_time timestamp ,eta_1_diff int , eta_1_time timestamp , eta_1_remark text , eta_2_diff int, eta_2_time timestamp , eta_2_remark text, eta_3_diff int ,  eta_3_time timestamp , eta_3_remark text,  PRIMARY KEY ((route_id, gen_date),route_seq,stop_seq,fetch_moment)) WITH CLUSTERING ORDER BY ( route_seq ASC,stop_seq ASC, fetch_moment DESC)  ;

