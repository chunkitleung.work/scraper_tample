from ast import excepthandler
from re import L
import requests
import csv
import json 
import datetime
import time
import copy
import numpy as np
from tqdm import tqdm
import shutil
import pandas as pd
import glob
import fnmatch
import os
from datetime import timedelta as timedelta



def update_GMB_routelist():

    if not os.path.exists("../data/minibus_info/{}".format(datetime.date.today())):
        os.mkdir( "../data/minibus_info/{}".format(datetime.date.today()))
    
    if not os.path.exists("../data/URL_list/{}".format(datetime.date.today() + datetime.timedelta(days=1))):
        os.mkdir( "../data/URL_list/{}".format(datetime.date.today() + datetime.timedelta(days=1)))

    if not os.path.exists("../data/URL_list/{}".format(datetime.date.today() )):
        os.mkdir( "../data/URL_list/{}".format(datetime.date.today()))

    try:
        res = requests.get("https://data.etagmb.gov.hk/route/")
    except:
        time.sleep(2)
        try:
            res = requests.get("https://data.etagmb.gov.hk/route/")
        except:
            time.sleep(4)
            try:
                res = requests.get("https://data.etagmb.gov.hk/route/")
            except:
                time.sleep(6)
                res = requests.get("https://data.etagmb.gov.hk/route/")
    
    
    route_list = res.json()
    output = []

    output.append({"Region":"HKI",
                            "routes":route_list['data']['routes']["HKI"]})
    
    output.append({"Region":"KLN",
                            "routes":route_list['data']['routes']["KLN"]})
    
    output.append({"Region":"NT",
                            "routes":route_list['data']['routes']["NT"]})

    for region in tqdm(output):
        for route in tqdm(range(len(region['routes']))):
            try:
                res2 = requests.get("https://data.etagmb.gov.hk/route/{}/{}".format(region['Region'],region['routes'][route]))
                routes_info = res2.json()


                region['routes'][route] = {"route_no":region['routes'][route],
                                                    "route_info":routes_info["data"]}
            
            except:
                time.sleep(2)
                try:
                    res2 = requests.get("https://data.etagmb.gov.hk/route/{}/{}".format(region['Region'],region['routes'][route]))
                    routes_info = res2.json()


                    region['routes'][route] = {"route_no":region['routes'][route],
                                                        "route_info":routes_info["data"]}
                
                except:
                    time.sleep(4)
                    try:
                        res2 = requests.get("https://data.etagmb.gov.hk/route/{}/{}".format(region['Region'],region['routes'][route]))
                        routes_info = res2.json()


                        region['routes'][route] = {"route_no":region['routes'][route],
                                                            "route_info":routes_info["data"]}
                    
                    except:
                        time.sleep(6)
                        try:
                            res2 = requests.get("https://data.etagmb.gov.hk/route/{}/{}".format(region['Region'],region['routes'][route]))
                            routes_info = res2.json()


                            region['routes'][route] = {"route_no":region['routes'][route],
                                                                "route_info":routes_info["data"]}
                    
                        except:
                            time.sleep(8)
                            res2 = requests.get("https://data.etagmb.gov.hk/route/{}/{}".format(region['Region'],region['routes'][route]))
                            routes_info = res2.json()


                            region['routes'][route] = {"route_no":region['routes'][route],
                                                                "route_info":routes_info["data"]}





    y = json.dumps(output)

    with open('../data/minibus_info/{}/GMB_info_{}.json'.format(datetime.date.today(),datetime.date.today()), 'w+') as f:
        f.write(y)

    print("finish part1 ")
    print()


def add_stopinfo():

    f = open('../data/minibus_info/{}/GMB_info_{}.json'.format(datetime.date.today(),datetime.date.today()))
    data = json.load(f)
    f.close()

    for region in tqdm(data):
        for route_no in tqdm(region['routes']):
            for route in route_no['route_info']:
                for direction in route["directions"]:

                    try:


                        res = requests.get("https://data.etagmb.gov.hk/route-stop/{}/{}".format(route["route_id"],direction["route_seq"]))
                        res = res.json()

                        direction["stop_info"] = res["data"]["route_stops"]

                    except:
                        time.sleep(2)

                        try:


                            res = requests.get("https://data.etagmb.gov.hk/route-stop/{}/{}".format(route["route_id"],direction["route_seq"]))
                            res = res.json()

                            direction["stop_info"] = res["data"]["route_stops"]

                        except:
                            time.sleep(4)

                            try:


                                res = requests.get("https://data.etagmb.gov.hk/route-stop/{}/{}".format(route["route_id"],direction["route_seq"]))
                                res = res.json()

                                direction["stop_info"] = res["data"]["route_stops"]

                            except:
                                time.sleep(6)

                                try:


                                    res = requests.get("https://data.etagmb.gov.hk/route-stop/{}/{}".format(route["route_id"],direction["route_seq"]))
                                    res = res.json()

                                    direction["stop_info"] = res["data"]["route_stops"]

                                except:
                                    time.sleep(8)

                                    try:


                                        res = requests.get("https://data.etagmb.gov.hk/route-stop/{}/{}".format(route["route_id"],direction["route_seq"]))
                                        res = res.json()

                                        direction["stop_info"] = res["data"]["route_stops"]

                                    except:
                                        time.sleep(10)

                                        try:


                                            res = requests.get("https://data.etagmb.gov.hk/route-stop/{}/{}".format(route["route_id"],direction["route_seq"]))
                                            res = res.json()

                                            direction["stop_info"] = res["data"]["route_stops"]

                                        except:
                                            time.sleep(12)

                                            try:


                                                res = requests.get("https://data.etagmb.gov.hk/route-stop/{}/{}".format(route["route_id"],direction["route_seq"]))
                                                res = res.json()

                                                direction["stop_info"] = res["data"]["route_stops"]

                                            except:
                                                time.sleep(14)

                                                res = requests.get("https://data.etagmb.gov.hk/route-stop/{}/{}".format(route["route_id"],direction["route_seq"]))
                                                res = res.json()

                                                direction["stop_info"] = res["data"]["route_stops"]



    y = json.dumps(data)

    with open('../data/minibus_info/{}/GMB_info_{}.json'.format(datetime.date.today(),datetime.date.today()), 'w+') as f:
        f.write(y)
    
    print("finish part 2")

def add_lat_long():

    f = open('../data/minibus_info/{}/GMB_info_{}.json'.format(datetime.date.today(),datetime.date.today()))
    data = json.load(f)
    f.close()

    for region in tqdm(data):
        for route_no in tqdm(region['routes']):
            for route in route_no['route_info']:
                for direction in route["directions"]:
                    for stop in direction["stop_info"]:

                        try:

                            res = requests.get("https://data.etagmb.gov.hk/stop/{}".format(stop["stop_id"]))
                            res = res.json()

                            stop["lat"] = res["data"]["coordinates"]["wgs84"]["latitude"]
                            stop["long"] = res["data"]["coordinates"]["wgs84"]["longitude"]
                        
                        except:
                            try:

                                res = requests.get("https://data.etagmb.gov.hk/stop/{}".format(stop["stop_id"]))
                                res = res.json()

                                stop["lat"] = res["data"]["coordinates"]["wgs84"]["latitude"]
                                stop["long"] = res["data"]["coordinates"]["wgs84"]["longitude"]
                            
                            except:
                                try:

                                    res = requests.get("https://data.etagmb.gov.hk/stop/{}".format(stop["stop_id"]))
                                    res = res.json()

                                    stop["lat"] = res["data"]["coordinates"]["wgs84"]["latitude"]
                                    stop["long"] = res["data"]["coordinates"]["wgs84"]["longitude"]
                            
                                except:
                                    try:

                                        res = requests.get("https://data.etagmb.gov.hk/stop/{}".format(stop["stop_id"]))
                                        res = res.json()

                                        stop["lat"] = res["data"]["coordinates"]["wgs84"]["latitude"]
                                        stop["long"] = res["data"]["coordinates"]["wgs84"]["longitude"]
                                    
                                    except:
                                        res = requests.get("https://data.etagmb.gov.hk/stop/{}".format(stop["stop_id"]))
                                        res = res.json()

                                        stop["lat"] = res["data"]["coordinates"]["wgs84"]["latitude"]
                                        stop["long"] = res["data"]["coordinates"]["wgs84"]["longitude"]


    y = json.dumps(data)


    with open('../data/minibus_info/{}/GMB_info_{}.json'.format(datetime.date.today(),datetime.date.today()), 'w+') as f:
        f.write(y)  
    
    print("finish part 3")
    

def gen_URLlist(j):

    f = open('../data/minibus_info/{}/GMB_info_{}.json'.format(datetime.date.today(),datetime.date.today()))
    data = json.load(f)
    f.close()

    URL_data = []

    for region in data:
        URL_list = []
        for route_no in region['routes']:
            for route in route_no['route_info']:
                for direction in route["directions"]:
                    for stop in direction["stop_info"]:
                        URL_list.append(["https://data.etagmb.gov.hk/eta/route-stop/" + str(route["route_id"]) + "/" + str(direction["route_seq"]) + "/" + str(stop["stop_seq"])])
        region_dist = {
            "region" : region["Region"],
            "URL" : copy.deepcopy(URL_list)
        }
        
        URL_data.append(copy.deepcopy(region_dist))

    
    splits = np.array_split(URL_data[0]["URL"], j)

    for i in range(j):

        with open('../data/URL_list/{}/GMB_URL_HKI_part{}_{}.csv'.format(datetime.date.today(),i+1,datetime.date.today()), 'w+', newline ='') as file:
            write = csv.writer(file)
            write.writerows(splits[i])
        

        with open('../data/URL_list/{}/GMB_URL_HKI_part{}_{}.csv'.format(datetime.date.today() +  datetime.timedelta(days=1) , i + 1 , datetime.date.today() + datetime.timedelta(days=1)) , 'w+', newline ='') as file2:
            write = csv.writer(file2)
            write.writerows(splits[i])



def midnight_routine():



    #create folder for today's data
    if not os.path.exists("../data/{}".format(datetime.date.today())):
        os.mkdir( "../data/{}".format(datetime.date.today()))

    try:
    #zip yesterday's data
        shutil.make_archive("../data/zipdata/{}".format(datetime.date.today()- datetime.timedelta(days=1)),'zip',"../data/{}/".format(datetime.date.today()- datetime.timedelta(days=1)))
    except:
        pass

    #remove the yesterday's data

    try:
        if os.path.exists("../data/zipdata/{}.zip".format(datetime.date.today()- datetime.timedelta(days=1))):
            shutil.rmtree("../data/{}".format(datetime.date.today()- datetime.timedelta(days=1)))

    except:
        pass

    #update the URL list for today
    update_GMB_routelist()
    add_stopinfo()
    gen_URLlist(6)
    # add_lat_long()
    print("Updated GMB list")




if __name__ == "__main__":

    # os.system("git add ../../ && git commit -m \" add {} data\" && git push".format(datetime.date.today()- datetime.timedelta(days=1))) 
    midnight_routine()
    # shutil.make_archive("../data/zipdata/{}".format(datetime.date.today()- datetime.timedelta(days=1)))
    # update_GMB_routelist()
    # add_stopinfo()
    # gen_URLlist(6)
