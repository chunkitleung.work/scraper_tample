from apscheduler.schedulers.blocking import BlockingScheduler
import subprocess
import datetime
import datetime
from multiprocessing import Process
from midnight_routine import *
from add_to_database import *
import time


def run_HKI_spider(part):
    # time.sleep(60)
    start_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    path = '../data/' + str(start_time).split()[0] + "/"+ str(start_time).split()[0] + '_' + str(start_time).split()[1].split('.')[0].replace(":","-") + '_HKI_part{}_temp.csv'.format(part)
    # subprocess.check_output(['scrapy', 'crawl', 'GMB_HKI_{}'.format(part),"-o", path,"-s","LOG_ENABLED=0"])
    subprocess.check_output(['scrapy', 'crawl', 'GMB_HKI_{}'.format(part),"-o", path,"-L","WARNING"])

    print("{}, HKI part{} done      Endtime : {}".format(start_time,part,datetime.datetime.now().strftime("%H:%M:%S")))

    add_to_database(start_time,part)

    print("{}, HKI part{} add datbase done      Endtime : {}".format(start_time,part,datetime.datetime.now().strftime("%H:%M:%S")))


def run_HKI_spider_mid(part):
    # time.sleep(60)
    start_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    path = '../data/' + str(datetime.date.today() - datetime.timedelta(days=1)) + "/"+str(start_time).split()[0] + '_' + str(start_time).split()[1].split('.')[0].replace(":","-") + '_HKI_part{}_temp.csv'.format(part)
    # subprocess.check_output(['scrapy', 'crawl', 'GMB_HKI_{}'.format(part),"-o", path,"-s","LOG_ENABLED=0"])
    subprocess.check_output(['scrapy', 'crawl', 'GMB_HKI_{}'.format(part),"-o", path,"-L","WARNING"])

    print("{}, HKI part{} done      Endtime : {}".format(start_time,part,datetime.datetime.now().strftime("%H:%M:%S")))

    add_to_database_mid(start_time,part)

    print("{}, HKI part{} add datbase done      Endtime : {}".format(start_time,part,datetime.datetime.now().strftime("%H:%M:%S")))



def _scheduler_job(input, part):

    sec = '*/15'
    scheduler = BlockingScheduler(timezone = "Asia/Hong_Kong")
    scheduler.add_job(input, args = part, trigger = 'cron', hour='5-23', minute='*', second = sec, max_instances = 500)

    scheduler.add_job(input, args = part, trigger = 'cron', hour='4', minute='30-59', second = sec, max_instances = 500)

    scheduler.add_job(run_HKI_spider_mid, args = part, trigger = 'cron', hour='0', minute='0-29', second = sec, max_instances = 500)

    scheduler.start()



def _scheduler_midnight():
    scheduler = BlockingScheduler(timezone = "Asia/Hong_Kong")
    scheduler.add_job(midnight_routine, trigger = 'cron', hour='0', minute='35', second = '00')
    scheduler.start()



if __name__ == "__main__":
    try:
   
        p1 = Process(target = _scheduler_job,args =(run_HKI_spider,'1'))
        p1.start()
        # p2 = Process(target = _scheduler_job,args = (run_HKI_spider,'2'))
        # p2.start()
        # p3 = Process(target = _scheduler_job,args = (run_HKI_spider,'3'))
        # p3.start()
        # p4 = Process(target = _scheduler_job,args = (run_HKI_spider,'4'))
        # p4.start()
        # p5 = Process(target = _scheduler_job,args = (run_HKI_spider,'5'))
        # p5.start()
        # p6 = Process(target = _scheduler_job,args = (run_HKI_spider,'6'))
        # p6.start()
        p7 = Process(target = _scheduler_midnight )
        p7.start()
    except KeyboardInterrupt:
        print("Program terminated manually!")
        raise SystemExit
