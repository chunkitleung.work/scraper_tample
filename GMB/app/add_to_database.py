# from cassandra.cluster import Cluster
from csv import DictReader
from csv import DictWriter
import os
from datetime import datetime as dt
import datetime


def add_column_in_csv(input_file, output_file, transform_row, tansform_column_names):
    """ Append a column in existing csv using csv.reader / csv.writer classes"""
    # Open the input_file in read mode and output_file in write mode
    with open(input_file, 'r') as read_obj, \
        open(output_file, 'w', newline='') as write_obj:
        # Create a DictReader object from the input file object
        dict_reader = DictReader(read_obj)
        # Get a list of column names from the csv
        field_names = dict_reader.fieldnames
        # Call the callback function to modify column name list
        tansform_column_names(field_names)
        # Create a DictWriter object from the output file object by passing column / field names
        dict_writer = DictWriter(write_obj, field_names)
        # Write the column names in output csv file
        dict_writer.writeheader()
        # Read each row of the input csv file as dictionary
        for row in dict_reader:
            # Modify the dictionary / row by passing it to the transform function (the callback)
            transform_row(row, dict_reader.line_num)
            # Write the updated dictionary or row to the output file
            dict_writer.writerow(row)




def add_to_database(start_time,part):

    host = "158.132.175.131"
    username = "cassandra"
    password = "cassandra"

    start_time_pathname = str(start_time).split()[0] + '_' + str(start_time).split()[1].split('.')[0].replace(":","-")

    add_column_in_csv("../data/{}/{}_HKI_part{}_temp.csv".format(str(start_time).split()[0],start_time_pathname,part),"../data/{}/{}_HKI_part{}.csv".format(str(start_time).split()[0],start_time_pathname,part),lambda row, line_num: row.update({'fetch_moment': dt.strptime(start_time,"%Y-%m-%d %H:%M:%S").strftime('%Y-%m-%d %H:%M:%S.%f%z+0800')}),lambda field_names: field_names.append('fetch_moment'))

    if os.path.isfile("../data/{}/{}_HKI_part{}_temp.csv".format(str(start_time).split()[0],start_time_pathname,part)):
        os.remove("../data/{}/{}_HKI_part{}_temp.csv".format(str(start_time).split()[0],start_time_pathname,part))

    query = " \"COPY eta_data (eta_1_diff, eta_1_remark, eta_1_time, eta_2_diff, eta_2_remark, eta_2_time, eta_3_diff, eta_3_remark, eta_3_time, gen_date, gen_time, route_id, route_seq, stop_seq, fetch_moment) FROM '../data/{}/{}_HKI_part{}.csv' WITH DELIMITER =',' AND HEADER = TRUE \" ".format(str(start_time).split()[0],start_time_pathname,int(part))

    command = f"""cqlsh {host} -u {username} -p {password} -k hki_gmb -e {query} """

    os.system(command)



def add_to_database_mid(start_time,part):

    host = "158.132.175.131"
    username = "cassandra"
    password = "cassandra"

    start_time_pathname = str(start_time).split()[0] + '_' + str(start_time).split()[1].split('.')[0].replace(":","-")

    add_column_in_csv("../data/{}/{}_HKI_part{}_temp.csv".format(str(datetime.date.today() - datetime.timedelta(days=1)),start_time_pathname,part),"../data/{}/{}_HKI_part{}.csv".format(str(datetime.date.today() - datetime.timedelta(days=1)),start_time_pathname,part),lambda row, line_num: row.update({'fetch_moment': dt.strptime(start_time,"%Y-%m-%d %H:%M:%S").strftime('%Y-%m-%d %H:%M:%S.%f%z+0800')}),lambda field_names: field_names.append('fetch_moment'))

    if os.path.isfile("../data/{}/{}_HKI_part{}_temp.csv".format(str(datetime.date.today() - datetime.timedelta(days=1)),start_time_pathname,part)):
        os.remove("../data/{}/{}_HKI_part{}_temp.csv".format(str(datetime.date.today() - datetime.timedelta(days=1)),start_time_pathname,part))

    query = " \"COPY eta_data (eta_1_diff, eta_1_remark, eta_1_time, eta_2_diff, eta_2_remark, eta_2_time, eta_3_diff, eta_3_remark, eta_3_time, gen_date, gen_time, route_id, route_seq, stop_seq, fetch_moment) FROM '../data/{}/{}_HKI_part{}.csv' WITH DELIMITER =',' AND HEADER = TRUE \" ".format(str(datetime.date.today() - datetime.timedelta(days=1)) , start_time_pathname,int(part))

    command = f"""cqlsh {host} -u {username} -p {password} -k hki_gmb -e {query} """

    os.system(command)




# session.execute("COPY eta_data (eta_1_diff, eta_1_remark, eta_1_time, eta_2_diff, eta_2_remark, eta_2_time, eta_3_diff, eta_3_remark, eta_3_time, gen_date, gen_time, route_id, route_seq, stop_seq, fetch_moment) FROM '../data/{}/{}_HKI_part{}.csv' WITH DELIMITER =',' AND HEADER = TRUE ;".format(str(start_time).split()[0],start_time_pathname,part))

# cluster = Cluster(['127.0.0.1'],
#             protocol_version = 5,
#             port=9042)

# session = cluster.connect("testing")


# process = subprocess.Popen("cqlsh " + host + " -u " + username + " -p " + password, shell=True)
# exitCode = process.wait()


# prepared = session.prepare("""
#     INSERT INTO eta_data (route_id, route_seq, stop_seq, gen_date, fetch_moment, gen_time, eta_1_diff, eta_1_time, eta_1_remark, eta_2_diff, eta_2_time, eta_2_remark, eta_3_diff, eta_3_time, eta_3_remark )
#     VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
#     """)



# with open ("../data/{}/{}_HKI_part{}.csv".format(str(start_time).split()[0],start_time_pathname,part),"r") as file:

#     csvreader = csv.reader(file)
#     next(csvreader) # skip header

#     for row in csvreader:

#         session.execute(prepared,[int(row[11]),int(row[12]),int(row[13]),dt.fromisoformat(row[9]),dt.fromisoformat(row[14]),dt.fromisoformat(row[10]),int(row[0]),dt.fromisoformat(row[2]),row[1],int(row[3]),dt.fromisoformat(row[5]),row[4],int(row[6]),dt.fromisoformat(row[8]),row[7]])


# session.execute("CREATE TABLE IF NOT EXISTS testing.eta_data (route_id int ,route_seq int , stop_seq int ,gen_date date, fetch_moment timestamp, gen_time timestamp ,eta_1_diff int , eta_1_time timestamp , eta_1_remark text , eta_2_diff int, eta_2_time timestamp , eta_2_remark text, eta_3_diff int ,  eta_3_time timestamp , eta_3_remark text,  PRIMARY KEY ((route_id, gen_date),route_seq,stop_seq,fetch_moment)) WITH CLUSTERING ORDER BY ( route_seq ASC,stop_seq ASC, fetch_moment DESC)")
# 
# 
# 
# 
# create table cql command;
# CREATE TABLE IF NOT EXISTS testing.eta_data (route_id int ,route_seq int , stop_seq int ,gen_date date, fetch_moment timestamp, gen_time timestamp ,eta_1_diff int , eta_1_time timestamp , eta_1_remark text , eta_2_diff int, eta_2_time timestamp , eta_2_remark text, eta_3_diff int ,  eta_3_time timestamp , eta_3_remark text,  PRIMARY KEY ((route_id, gen_date),route_seq,stop_seq,fetch_moment)) WITH CLUSTERING ORDER BY ( route_seq ASC,stop_seq ASC, fetch_moment DESC)  


