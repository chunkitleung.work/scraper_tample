# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

class GmbPipeline:
    def process_item(self, item, spider):
        return item


from cassandra.cluster import Cluster


def convert_none(odj):
    if odj == None:
        return 'null'
    else:
        return str(odj)

def convert_none_time(odj):
    if odj == None:
        return 'null'
    else:
        return "\'" + str(odj) + "\'"

class CassandraPipeline(object):
    
    def __init__(self,cassandra_keyspace):
        self.cassandra_keyspace = cassandra_keyspace
    
    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            cassandra_keyspace='testing'
        )

    def open_spider(self, spider):

        cluster = Cluster(['127.0.0.1'],
        # load_balancing_policy=DCAwareRoundRobinPolicy(local_dc='US_EAST'),
        protocol_version = 5,
        port=9042)
        self.session = cluster.connect(self.cassandra_keyspace)

        # create scrapy_items table

        self.session.execute("CREATE TABLE IF NOT EXISTS " + self.cassandra_keyspace + ".GMB_data ( route_id int ,route_seq int , stop_seq int, gen_time timestamp ,eta_1_diff int , eta_1_time timestamp , eta_1_remark text , eta_2_diff int, eta_2_time timestamp , eta_2_remark text, eta_3_diff int ,  eta_3_time timestamp , eta_3_remark text,  PRIMARY KEY ((route_id),route_seq,stop_seq,gen_time)) WITH CLUSTERING ORDER BY ( route_seq ASC,stop_seq ASC)")  
        # self.session.execute("CREATE TABLE IF NOT EXISTS " + self.cassandra_keyspace + ".GMB_data ( item text, PRIMARY KEY (item))")



    def process_item(self, item, spider):

        self.session.execute("INSERT INTO gmb_data (route_id, route_seq, stop_seq, gen_time, eta_1_diff, eta_1_time, eta_1_remark, eta_2_diff, eta_2_time, eta_2_remark, eta_3_diff, eta_3_time, eta_3_remark ) VALUES ({},{},{},{},{},{},{},{},{},{},{},{},{})".format( item['route_id'] ,  item['route_seq']  ,  item['stop_seq']  , "\'" + item['gen_time'] + "\'" , convert_none(item['eta_1_diff']) ,   convert_none_time(item['eta_1_time']),convert_none_time(item['eta_1_remark']), convert_none(item['eta_2_diff'] ) ,  convert_none_time(item['eta_2_time']) , convert_none_time(item['eta_2_remark']), convert_none(item['eta_3_diff']) ,  convert_none_time(item['eta_3_time']) , convert_none_time(item['eta_3_remark']) ))
        
        
        # a = self.session.execute("DESCRIBE TABLES")

        # for b in range(a):
        #     print(b)

        return item

