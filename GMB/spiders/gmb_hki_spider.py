import scrapy
import json
from scrapy.crawler import CrawlerProcess
from datetime import datetime as dt
import logging 
from scrapy.utils.log import configure_logging 
from GMB.items import GmbItem
import datetime

class Gmbhki1Spider(scrapy.Spider):

    name = 'GMB_HKI_1'
    allowed_domains = ['data.etagmb.gov.hk/eta/route-stop/']
    start_urls = [l.strip() for l in open('../data/URL_list/{}/GMB_URL_HKI_part1_{}.csv'.format(datetime.date.today(),datetime.date.today())).readlines()]
    # start_urls = ["https://data.etagmb.gov.hk/eta/route-stop/2006408/1/1"]

    def parse(self, response):

        res = json.loads(response.text)

        item = GmbItem()


        if res['data']['eta'] == []:

            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] =  dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = None
            item['eta_1_time'] = None
            item['eta_1_remark'] = None
            item['eta_2_diff'] = None
            item['eta_2_time'] = None
            item['eta_2_remark'] = None
            item['eta_3_time'] = None
            item['eta_3_diff'] = None
            item['eta_3_remark'] = None

            
        elif len(res['data']['eta'])==2:

            item['route_id'] =  response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = res['data']['eta'][1]['diff']
            item['eta_2_time'] = dt.fromisoformat(res['data']['eta'][1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_2_remark'] = res['data']['eta'][1]['remarks_en']
            item['eta_3_diff'] = None
            item['eta_3_time'] = None
            item['eta_3_remark'] = None
            
        
        elif len(res['data']['eta'])==1:


            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] =res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = None
            item['eta_2_time'] = None
            item['eta_2_remark'] = None
            item['eta_3_diff'] = None
            item['eta_3_time'] = None
            item['eta_3_remark'] = None


        else:

            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = res['data']['eta'][1]['diff']
            item['eta_2_time'] =  dt.fromisoformat(res['data']['eta'][1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_2_remark'] = res['data']['eta'][1]['remarks_en']
            item['eta_3_diff'] = res['data']['eta'][2]['diff']
            item['eta_3_time'] = dt.fromisoformat(res['data']['eta'][2]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_3_remark'] = res['data']['eta'][2]['remarks_en']

        yield item





class Gmbhki2Spider(scrapy.Spider):

    name = 'GMB_HKI_2'
    allowed_domains = ['data.etagmb.gov.hk/eta/route-stop/']
    start_urls = [l.strip() for l in open('../data/URL_list/{}/GMB_URL_HKI_part2_{}.csv'.format(datetime.date.today(),datetime.date.today())).readlines()]
    # start_urls = ["https://data.etagmb.gov.hk/eta/route-stop/2006408/1/1"]

    def parse(self, response):

        res = json.loads(response.text)

        item = GmbItem()


        if res['data']['eta'] == []:

            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] =  dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = None
            item['eta_1_time'] = None
            item['eta_1_remark'] = None
            item['eta_2_diff'] = None
            item['eta_2_time'] = None
            item['eta_2_remark'] = None
            item['eta_3_time'] = None
            item['eta_3_diff'] = None
            item['eta_3_remark'] = None

            
        elif len(res['data']['eta'])==2:

            item['route_id'] =  response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = res['data']['eta'][1]['diff']
            item['eta_2_time'] = dt.fromisoformat(res['data']['eta'][1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_2_remark'] = res['data']['eta'][1]['remarks_en']
            item['eta_3_diff'] = None
            item['eta_3_time'] = None
            item['eta_3_remark'] = None
            
        
        elif len(res['data']['eta'])==1:


            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] =res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = None
            item['eta_2_time'] = None
            item['eta_2_remark'] = None
            item['eta_3_diff'] = None
            item['eta_3_time'] = None
            item['eta_3_remark'] = None


        else:

            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = res['data']['eta'][1]['diff']
            item['eta_2_time'] =  dt.fromisoformat(res['data']['eta'][1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_2_remark'] = res['data']['eta'][1]['remarks_en']
            item['eta_3_diff'] = res['data']['eta'][2]['diff']
            item['eta_3_time'] = dt.fromisoformat(res['data']['eta'][2]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_3_remark'] = res['data']['eta'][2]['remarks_en']

        yield item
                 



class Gmbhki3Spider(scrapy.Spider):

    name = 'GMB_HKI_3'
    allowed_domains = ['data.etagmb.gov.hk/eta/route-stop/']
    start_urls = [l.strip() for l in open('../data/URL_list/{}/GMB_URL_HKI_part3_{}.csv'.format(datetime.date.today(),datetime.date.today())).readlines()]
    # start_urls = ["https://data.etagmb.gov.hk/eta/route-stop/2006408/1/1"]

    def parse(self, response):

        res = json.loads(response.text)

        item = GmbItem()


        if res['data']['eta'] == []:

            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] =  dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = None
            item['eta_1_time'] = None
            item['eta_1_remark'] = None
            item['eta_2_diff'] = None
            item['eta_2_time'] = None
            item['eta_2_remark'] = None
            item['eta_3_time'] = None
            item['eta_3_diff'] = None
            item['eta_3_remark'] = None

            
        elif len(res['data']['eta'])==2:

            item['route_id'] =  response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = res['data']['eta'][1]['diff']
            item['eta_2_time'] = dt.fromisoformat(res['data']['eta'][1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_2_remark'] = res['data']['eta'][1]['remarks_en']
            item['eta_3_diff'] = None
            item['eta_3_time'] = None
            item['eta_3_remark'] = None
            
        
        elif len(res['data']['eta'])==1:


            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] =res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = None
            item['eta_2_time'] = None
            item['eta_2_remark'] = None
            item['eta_3_diff'] = None
            item['eta_3_time'] = None
            item['eta_3_remark'] = None


        else:

            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = res['data']['eta'][1]['diff']
            item['eta_2_time'] =  dt.fromisoformat(res['data']['eta'][1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_2_remark'] = res['data']['eta'][1]['remarks_en']
            item['eta_3_diff'] = res['data']['eta'][2]['diff']
            item['eta_3_time'] = dt.fromisoformat(res['data']['eta'][2]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_3_remark'] = res['data']['eta'][2]['remarks_en']

        yield item
                 



class Gmbhki4Spider(scrapy.Spider):

    name = 'GMB_HKI_4'
    allowed_domains = ['data.etagmb.gov.hk/eta/route-stop/']
    start_urls = [l.strip() for l in open('../data/URL_list/{}/GMB_URL_HKI_part4_{}.csv'.format(datetime.date.today(),datetime.date.today())).readlines()]
    # start_urls = ["https://data.etagmb.gov.hk/eta/route-stop/2006408/1/1"]

    def parse(self, response):

        res = json.loads(response.text)

        item = GmbItem()


        if res['data']['eta'] == []:

            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] =  dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = None
            item['eta_1_time'] = None
            item['eta_1_remark'] = None
            item['eta_2_diff'] = None
            item['eta_2_time'] = None
            item['eta_2_remark'] = None
            item['eta_3_time'] = None
            item['eta_3_diff'] = None
            item['eta_3_remark'] = None

            
        elif len(res['data']['eta'])==2:

            item['route_id'] =  response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = res['data']['eta'][1]['diff']
            item['eta_2_time'] = dt.fromisoformat(res['data']['eta'][1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_2_remark'] = res['data']['eta'][1]['remarks_en']
            item['eta_3_diff'] = None
            item['eta_3_time'] = None
            item['eta_3_remark'] = None
            
        
        elif len(res['data']['eta'])==1:


            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] =res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = None
            item['eta_2_time'] = None
            item['eta_2_remark'] = None
            item['eta_3_diff'] = None
            item['eta_3_time'] = None
            item['eta_3_remark'] = None


        else:

            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = res['data']['eta'][1]['diff']
            item['eta_2_time'] =  dt.fromisoformat(res['data']['eta'][1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_2_remark'] = res['data']['eta'][1]['remarks_en']
            item['eta_3_diff'] = res['data']['eta'][2]['diff']
            item['eta_3_time'] = dt.fromisoformat(res['data']['eta'][2]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_3_remark'] = res['data']['eta'][2]['remarks_en']

        yield item
                 



class Gmbhki5Spider(scrapy.Spider):

    name = 'GMB_HKI_5'
    allowed_domains = ['data.etagmb.gov.hk/eta/route-stop/']
    start_urls = [l.strip() for l in open('../data/URL_list/{}/GMB_URL_HKI_part5_{}.csv'.format(datetime.date.today(),datetime.date.today())).readlines()]
    # start_urls = ["https://data.etagmb.gov.hk/eta/route-stop/2006408/1/1"]

    def parse(self, response):

        res = json.loads(response.text)

        item = GmbItem()


        if res['data']['eta'] == []:

            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] =  dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = None
            item['eta_1_time'] = None
            item['eta_1_remark'] = None
            item['eta_2_diff'] = None
            item['eta_2_time'] = None
            item['eta_2_remark'] = None
            item['eta_3_time'] = None
            item['eta_3_diff'] = None
            item['eta_3_remark'] = None

            
        elif len(res['data']['eta'])==2:

            item['route_id'] =  response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = res['data']['eta'][1]['diff']
            item['eta_2_time'] = dt.fromisoformat(res['data']['eta'][1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_2_remark'] = res['data']['eta'][1]['remarks_en']
            item['eta_3_diff'] = None
            item['eta_3_time'] = None
            item['eta_3_remark'] = None
            
        
        elif len(res['data']['eta'])==1:


            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] =res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = None
            item['eta_2_time'] = None
            item['eta_2_remark'] = None
            item['eta_3_diff'] = None
            item['eta_3_time'] = None
            item['eta_3_remark'] = None


        else:

            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = res['data']['eta'][1]['diff']
            item['eta_2_time'] =  dt.fromisoformat(res['data']['eta'][1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_2_remark'] = res['data']['eta'][1]['remarks_en']
            item['eta_3_diff'] = res['data']['eta'][2]['diff']
            item['eta_3_time'] = dt.fromisoformat(res['data']['eta'][2]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_3_remark'] = res['data']['eta'][2]['remarks_en']

        yield item
                 




class Gmbhki6Spider(scrapy.Spider):

    name = 'GMB_HKI_6'
    allowed_domains = ['data.etagmb.gov.hk/eta/route-stop/']
    start_urls = [l.strip() for l in open('../data/URL_list/{}/GMB_URL_HKI_part6_{}.csv'.format(datetime.date.today(),datetime.date.today())).readlines()]
    # start_urls = ["https://data.etagmb.gov.hk/eta/route-stop/2006408/1/1"]

    def parse(self, response):

        res = json.loads(response.text)

        item = GmbItem()


        if res['data']['eta'] == []:

            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] =  dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = None
            item['eta_1_time'] = None
            item['eta_1_remark'] = None
            item['eta_2_diff'] = None
            item['eta_2_time'] = None
            item['eta_2_remark'] = None
            item['eta_3_time'] = None
            item['eta_3_diff'] = None
            item['eta_3_remark'] = None

            
        elif len(res['data']['eta'])==2:

            item['route_id'] =  response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = res['data']['eta'][1]['diff']
            item['eta_2_time'] = dt.fromisoformat(res['data']['eta'][1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_2_remark'] = res['data']['eta'][1]['remarks_en']
            item['eta_3_diff'] = None
            item['eta_3_time'] = None
            item['eta_3_remark'] = None
            
        
        elif len(res['data']['eta'])==1:


            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] =res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = None
            item['eta_2_time'] = None
            item['eta_2_remark'] = None
            item['eta_3_diff'] = None
            item['eta_3_time'] = None
            item['eta_3_remark'] = None


        else:

            item['route_id'] = response.request.url.split("/")[-3]
            item['route_seq'] = response.request.url.split("/")[-2]
            item['stop_seq'] = response.request.url.split("/")[-1]
            item['gen_date'] = datetime.datetime.now().date()
            item['gen_time'] = dt.fromisoformat(res['generated_timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_diff'] = res['data']['eta'][0]['diff']
            item['eta_1_time'] = dt.fromisoformat(res['data']['eta'][0]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_1_remark'] = res['data']['eta'][0]['remarks_en']
            item['eta_2_diff'] = res['data']['eta'][1]['diff']
            item['eta_2_time'] =  dt.fromisoformat(res['data']['eta'][1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_2_remark'] = res['data']['eta'][1]['remarks_en']
            item['eta_3_diff'] = res['data']['eta'][2]['diff']
            item['eta_3_time'] = dt.fromisoformat(res['data']['eta'][2]['timestamp']).strftime('%Y-%m-%d %H:%M:%S.%f%z')
            item['eta_3_remark'] = res['data']['eta'][2]['remarks_en']

        yield item
                 

                 
